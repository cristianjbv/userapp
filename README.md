# Global Logic TEST

URL_BASE: http://localhost:8081


Requerimientos Mínimos: Para ejecutar esta aplicación debes tener instalado un JDK 8.

Para su Ejecución:

Puede abrir un terminal en la raíz del proyecto y usar alguno de 
los siguientes métodos: 

 - "./gradlew bootRun"   [sin comillas, remover "./" en windows.]
 
 - Si deseas generar un jar ejecutable puedes usar el comando "./gradlew bootJar", 
 el archivo jar se almacenará en /build/libs/userapp-0.0.1-SNAPSHOT.jar
 Una vez que el archivo está empaquetado en el jar sólo debes ejecutar 
 "java -jar /build/libs/userapp-0.0.1-SNAPSHOT.jar" y la aplicación se ejecutará.
 
#Ejecución mediante Docker

Si desea ejecutar mediante docker, debe abrir una consola en la raíz del proyecto y ejecutar los siguientes comandos:

 * docker build -t gl/userapp .
 * docker run -p 8081:8081 gl/userapp
 
 podrá realizar consultas  a la aplicación en la ruta: http://localhost:8081
 
# Instrucciones para Endpoints y funcionalidades del sistema.


- El sistema cuenta con las operaciones básicas (CRUD)  de la entidad User, si desea ver documentación más completa de 
    los endpoints puedes acceder a swagger con la ruta (La aplicación debe estar ejecutándose):
    http://localhost:8081/swagger-ui/index.html

- Las principales rutas son: 
 *Authenticate: http://localhost:8081/api/authenticate
    Esta ruta recibe un json en el siguiente formato: 
    {
        "username":"admin@globallogic.com",
        "password":"Hunter22"
    }
    
 *Create User: POST  http://localhost:8081/api/users       
    La petición debe contener un json con la siguiente estructura  para poder crear un usuario.
    
 {
 	"name": "Nuevo Usuario",
 	"email": "nuevo2@globallogic.com",
 	"password":"1234Aa",
 	"active": "true",
 	"phones": [
 				{
 					"number": 772954625,
 					"cityCode": 9,
 					"countryCode": 56
 				}
 			]
 }
 
 #Testing
 
    * La actual rama (master) posee pruebas unitarias que funcionan con Junit 5 y Mockito, sin embargo, 
    una segunda rama llamada "feature/spock" tendrá disponibles las dependencias para ejecutar algunas
    pruebas con spock, con la intención de demostrar su implementación. (Realizar checkout en rama 
    para realizar test con spock.) 
 
    * para la ejecución de las pruebas mediante la IDE sólo es necesario inicializar la 
    función Run as Junit (Eclipse / STS) o simplemente Run desde Intellij Idea.