FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/*.jar build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]