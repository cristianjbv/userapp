package com.globallogic.userapp.service;

import com.globallogic.userapp.config.jwt.JwtTokenUtil;
import com.globallogic.userapp.domain.dto.LoginDTO;
import com.globallogic.userapp.repository.UserRepository;
import com.globallogic.userapp.service.impl.AuthServiceImpl;
import com.globallogic.userapp.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
class AuthServiceTest {

    private AuthService authService;
    //Class dependencies
    @MockBean
    private AuthenticationManager authenticationManagerMock;
    @MockBean
    private JwtTokenUtil jwtTokenUtilMock;
    @MockBean
    private UserService userServiceMock;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private ModelMapper mapper;
    @MockBean
    private PasswordEncoder encoder;

    //Common variables



    private static final String EMAIL = "admin@globallogic.com";
    private static final String PASSWORD = "Hunter22";
    private static final String TOKEN = "asdf1234";
    private static final String JSON_RESPONSE = "{\"token\":\"" + TOKEN + "\"}";


    @BeforeEach
    void setup() {
        Optional<com.globallogic.userapp.domain.User> optionalUser =  Optional.of(getMockedUser());

        UserDetails userDetailsMock = new User(EMAIL,PASSWORD,new ArrayList<>());
        Authentication authenticateMock = Mockito.mock(Authentication.class);
        Mockito.when(authenticationManagerMock.authenticate(Mockito.any(Authentication.class))).thenReturn(authenticateMock);
        Mockito.when(authenticateMock.getPrincipal()).thenReturn(userDetailsMock);
        User user = new User(EMAIL, PASSWORD, new ArrayList<>());
        Mockito.when(jwtTokenUtilMock.generateAccessToken(user)).thenReturn(TOKEN);


        Mockito.when(userRepository.findOneByEmailIgnoreCase(EMAIL)).thenReturn(optionalUser);
        userServiceMock = new UserServiceImpl(userRepository, mapper, encoder);
        authService = new AuthServiceImpl(authenticationManagerMock, jwtTokenUtilMock, userServiceMock);
    }

    @Test
    void authenticateTest(){
        LoginDTO loginDTO = createDummyLoginDTO();
        String jwt = authService.authenticate(loginDTO);
        Assertions.assertTrue( StringUtils.isNotBlank(jwt));
    }

    private LoginDTO createDummyLoginDTO(){
        return new LoginDTO(EMAIL, PASSWORD);
    }

    private com.globallogic.userapp.domain.User getMockedUser() {
        return new com.globallogic.userapp.domain.User(1L, "test", EMAIL, null, Instant.now(), null, null, null, true, null);
    }
}
