package com.globallogic.userapp.web.rest;

import com.globallogic.userapp.config.jwt.JwtTokenUtil;
import com.globallogic.userapp.domain.dto.LoginDTO;
import com.globallogic.userapp.repository.UserRepository;
import com.globallogic.userapp.service.AuthService;
import com.globallogic.userapp.service.UserService;
import com.globallogic.userapp.service.impl.CustomUserDetailServiceImpl;
import com.globallogic.userapp.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.security.Principal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthResource.class)
class AuthResourceTest {

    @Autowired
    private MockMvc mvc;



    @MockBean
    private AuthenticationManager authenticationManagerMock;
    @MockBean
    private JwtTokenUtil jwtTokenUtilMock;
    @MockBean
    private UserService userServiceMock;
    @MockBean
    private CustomUserDetailServiceImpl customUserDetailServiceMock;
    @MockBean
    private UserRepository userRepositoryMock;
    @MockBean
    private AuthService authService;
    @Mock
    private Authentication authenticateMock;
    @Mock
    private Principal principalMock;
    @Mock
    private ModelMapper mockedModelMapper;
    @Mock
    private PasswordEncoder mockedEncoder;

    //Constants
    private static final String TOKEN = "asdf1234";
    private static final String JSON_RESPONSE = "{\"token\":\"" + TOKEN + "\"}";

    private Logger logger = LoggerFactory.getLogger(AuthResourceTest.class);

    @BeforeEach
    void setUp(){
        userServiceMock = new UserServiceImpl(userRepositoryMock, mockedModelMapper, mockedEncoder);
        Mockito.when(authService.authenticate(Mockito.any(LoginDTO.class))).thenReturn(TOKEN);
    }
    @Test
    void authenticateTest() {

        try {
            mvc.perform(MockMvcRequestBuilders.post("/api/authenticate")
                    .content("{\"username\":\"admin@globallogic.com\",\"password\":\"Hunter22\"}")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().json(JSON_RESPONSE))
                    .andExpect(status().isOk());

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

    }

}
