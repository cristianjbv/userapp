package com.globallogic.userapp.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globallogic.userapp.config.jwt.JwtTokenUtil;
import com.globallogic.userapp.domain.User;
import com.globallogic.userapp.domain.dto.UserDTO;
import com.globallogic.userapp.repository.UserRepository;
import com.globallogic.userapp.service.AuthService;
import com.globallogic.userapp.service.UserService;
import com.globallogic.userapp.service.impl.CustomUserDetailServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserResource.class)
@AutoConfigureMockMvc
class UserResourceTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private JwtTokenUtil jwtTokenUtil;
    @MockBean
    private CustomUserDetailServiceImpl customUserDetailService;
    @MockBean
    private UserService userService;
    @MockBean
    private ModelMapper mapper;
    @MockBean
    private AuthService authService;

    private UserDTO mockedUserDTO;


    private final static String EMAIL = "admin@globallogic.com";
    private final static String NAME = "John Doe";
    private final static String JWT = "Bearer token";
    private final static String URL = "/api/users";


    @BeforeEach
    void setUp() {
        mockedUserDTO = this.getMockedUserDTO();
        List<UserDTO> mockedListDTO = Arrays.asList(mockedUserDTO);
        Page<UserDTO> mockedPageDTO = Mockito.mock(Page.class);
        UserDetails mockedUserDetails = Mockito.mock(UserDetails.class);
        Mockito.when(mockedUserDetails.getAuthorities()).thenReturn(new ArrayList<>());
        Mockito.when(mockedUserDetails.getUsername()).thenReturn(EMAIL);
        Mockito.when(jwtTokenUtil.validate(Mockito.anyString())).thenReturn(true);
        Mockito.when(jwtTokenUtil.getUsername(Mockito.anyString())).thenReturn(EMAIL);
        Mockito.when(customUserDetailService.loadUserByUsername(Mockito.anyString())).thenReturn(mockedUserDetails);
        Mockito.when(userService.getAllUsers(Mockito.any(Pageable.class))).thenReturn(mockedPageDTO);
        Mockito.when(mapper.map(Mockito.any(User.class), Mockito.any())).thenReturn(mockedUserDTO);
        Mockito.when(mockedPageDTO.getContent()).thenReturn(mockedListDTO);
    }

    @Test
    void testGetAllusers() throws Exception {
        ResultActions resultActions = this.mvc.perform(get(URL).header("Authorization", JWT))
                .andExpect(status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Assert.assertNotNull("Non-Empty User DTO List", contentAsString);
        List<UserDTO> userDTOList = objectMapper.readValue(contentAsString, List.class);
        Assert.assertFalse("Non-Empty User DTO List", userDTOList.isEmpty());
    }

    @Test
    void testGetUser() throws Exception {
        Mockito.when(userService.getUserById(Mockito.anyLong())).thenReturn(mockedUserDTO);
        ResultActions resultActions = this.mvc.perform(get(URL + "/1").header("Authorization", JWT))
                .andExpect(status().isOk());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Assert.assertNotNull("Non-Empty User DTO ", contentAsString);
        UserDTO userDTO = objectMapper.readValue(contentAsString, UserDTO.class);
        Assert.assertEquals("User received", userDTO, mockedUserDTO);
    }

    @Test
    void testDeleteUser() throws Exception {
        ResultActions resultActions = this.mvc.perform(delete(URL + "/1").header("Authorization", JWT))
                .andExpect(status().isNoContent());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Assert.assertTrue("Delete, No Content", StringUtils.isBlank(contentAsString));
    }


    @Test
    void testCreateUser() throws Exception {
        Mockito.when(userService.createUser(Mockito.any(UserDTO.class))).thenReturn(mockedUserDTO);
        ResultActions resultActions = this.mvc.perform(post(URL).header("Authorization", JWT)
                .content(objectMapper.writeValueAsString(mockedUserDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Assert.assertNotNull("Non-Empty User DTO ", contentAsString);
        UserDTO userDTO = objectMapper.readValue(contentAsString, UserDTO.class);
        Assert.assertEquals("User created", userDTO, mockedUserDTO);
    }

    private UserDTO getMockedUserDTO() {
        return new UserDTO(1L, NAME, EMAIL, "Hunter22", Instant.now(), null, null, null, true, null);
    }




}
