package com.globallogic.userapp.service.impl

import com.globallogic.userapp.domain.User
import com.globallogic.userapp.domain.dto.UserDTO
import com.globallogic.userapp.repository.UserRepository
import com.globallogic.userapp.service.UserService
import org.mockito.Mockito
import org.modelmapper.ModelMapper
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

import java.time.Instant

class UserServiceImplTest extends Specification {

    //Class to be tested
    private UserService userService

    //Dependencies
    private UserRepository mockedUserRepository
    private ModelMapper mockedModelMapper
    private PasswordEncoder mockedPasswordEncoder

    //Common variables
    private UserDTO userDTO
    Pageable mockedPageable

    void setup(){
        mockedUserRepository =  Stub(UserRepository.class)
        mockedModelMapper = Stub(ModelMapper.class)
        mockedPasswordEncoder = Stub(PasswordEncoder.class)
        userDTO  = new UserDTO(1L, "test", "admin@globallogic.com", "Hunter22",
                Instant.now(), null, null, null, true, null)
        mockedPageable = Mock()
        //initializing testing class
        userService = new UserServiceImpl(mockedUserRepository, mockedModelMapper, mockedPasswordEncoder)
   }


    void "getting all users from UserService "() {
        given: "a dummy UserDTO List"
        List<UserDTO> mockedUserListDTO = new LinkedList<>()
        mockedUserListDTO.add(userDTO)
        Page<UserDTO> mockedPage = Mock()


        and: "a mocked UserRepository that always return this user inside a List"
        mockedUserRepository.findAll(Pageable.class, Mockito.any(Pageable.class)) >> mockedUserListDTO
        mockedModelMapper.map(Mockito.any(User.class), UserDTO.class) >> userDTO
        mockedPage.getContent() >> mockedUserListDTO

        when: "we request the user list"
        Page<UserDTO> response = userService.getAllUsers(mockedPageable)

        then: "we got a page of UserDTO"
        response.getContent() != null
    }

    void "database is empty"(){
        List<UserDTO> mockedUserListDTO = new LinkedList<>()
        given: "The DB has no records stored"
        mockedUserRepository.findAll(Pageable.class,Mockito.any(Pageable.class)) >> mockedUserListDTO

        when: "We request all the users"
        Page<UserDTO> response = userService.getAllUsers(mockedPageable)

        then: "we got an empty list (null)"
        response.getContent().isEmpty()
    }
}
