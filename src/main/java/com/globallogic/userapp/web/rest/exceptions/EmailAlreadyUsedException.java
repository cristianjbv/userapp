package com.globallogic.userapp.web.rest.exceptions;

public class EmailAlreadyUsedException extends Exception {

    public EmailAlreadyUsedException(String message) {
        super(message);
    }

}
