package com.globallogic.userapp.web.rest.exceptions;



public final class ExceptionConstants {

    public static final String NEW_USER_WITH_ID_EXCEPTION = "New users cannot contain an ID";
    public static final String USER_ALREADY_EXISTS_EXCEPTION = "Your email is already registered";
    public static final String ENTITY_NOT_FOUND_EXCEPTION = "Incorrect user ID";

    private ExceptionConstants(){}
}
