package com.globallogic.userapp.web.rest;

import com.globallogic.userapp.domain.dto.AuthResponseDTO;
import com.globallogic.userapp.domain.dto.LoginDTO;
import com.globallogic.userapp.service.AuthService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthResource {

   private final AuthService authService;

    @ApiOperation(value = "Authenticates an User and returns a JWT")
    @PostMapping("/authenticate")
    public ResponseEntity<AuthResponseDTO> authenticate(@RequestBody LoginDTO loginDTO) {
        String jwt = authService.authenticate(loginDTO);
        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, "bearer " + jwt)
                .body(new AuthResponseDTO(jwt));
    }
}
