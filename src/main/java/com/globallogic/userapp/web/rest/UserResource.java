package com.globallogic.userapp.web.rest;

import com.globallogic.userapp.domain.dto.UserDTO;
import com.globallogic.userapp.service.UserService;
import com.globallogic.userapp.web.rest.exceptions.EmailAlreadyUsedException;
import com.globallogic.userapp.web.rest.exceptions.UserAlreadyExistsException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserResource {

    private static final List<String> ALLOWED_ORDERED_PROPERTIES = Collections.unmodifiableList(
            Arrays.asList("id", "email", "name"));

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    @PostMapping("/users")
    @ApiOperation(
            value="Creates a new User",
            authorizations = {
            @Authorization(value = "Only the user that submitted the request can get it")
    })
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException, UserAlreadyExistsException {
        log.debug("REST request to create User : {}", userDTO);
        UserDTO newUserDTO = this.userService.createUser(userDTO);
        return ResponseEntity
                .created(new URI("/api/users/" + newUserDTO.getId()))
                .body(newUserDTO);
    }

    @PutMapping("/users")
    @ApiOperation(value="Updates an existing User")
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) throws EmailAlreadyUsedException {
        log.debug("REST request to update User : {}", userDTO);
        UserDTO updatedUserDTO = this.userService.updateUser(userDTO);
        return ResponseEntity.ok()
                .body(updatedUserDTO);
    }

    @DeleteMapping("/users/{id}")
    @ApiOperation(value="Deletes an existing User")
    public ResponseEntity<UserDTO> deleteUser(@PathVariable long id) {
        log.debug("REST request to delete User with id : {}", id);
        this.userService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/users")
    @ApiOperation(value="return all Users")
    public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
        if (!onlyContainsAllowedProperties(pageable)) {
            return ResponseEntity.badRequest().build();
        }
        final Page<UserDTO> page = userService.getAllUsers(pageable);
        return new ResponseEntity<>(page.getContent(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    @ApiOperation(value="Display specific User by id")
    public ResponseEntity<UserDTO> getUserById(@PathVariable long id) {
        return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
    }

    private boolean onlyContainsAllowedProperties(Pageable pageable) {
        return pageable.getSort().stream().map(Sort.Order::getProperty).allMatch(ALLOWED_ORDERED_PROPERTIES::contains);
    }

}
