package com.globallogic.userapp.service.impl;

import com.globallogic.userapp.config.jwt.JwtTokenUtil;
import com.globallogic.userapp.domain.dto.LoginDTO;
import com.globallogic.userapp.service.AuthService;
import com.globallogic.userapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;

    @Override
    public String authenticate(LoginDTO loginDTO) {
        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
        //Getting user from Principal, avoiding an extra DB call.
        User user = (User) authenticate.getPrincipal();
        String jwt = jwtTokenUtil.generateAccessToken(user);
        this.userService.syncUserData(jwt, user.getUsername());
        return jwt;
    }
}
