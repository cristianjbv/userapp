package com.globallogic.userapp.service.impl;

import com.globallogic.userapp.domain.User;
import com.globallogic.userapp.domain.dto.UserDTO;
import com.globallogic.userapp.repository.UserRepository;
import com.globallogic.userapp.service.UserService;
import com.globallogic.userapp.web.rest.exceptions.BadRequestException;
import com.globallogic.userapp.web.rest.exceptions.EmailAlreadyUsedException;
import com.globallogic.userapp.web.rest.exceptions.ExceptionConstants;
import com.globallogic.userapp.web.rest.exceptions.UserAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder encoder;

    @Override
    public UserDTO createUser(UserDTO userDTO) throws UserAlreadyExistsException {
        if (Objects.nonNull(userDTO.getId())) {
            throw new BadRequestException(ExceptionConstants.NEW_USER_WITH_ID_EXCEPTION);
        } else if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()) {
            throw new UserAlreadyExistsException(ExceptionConstants.USER_ALREADY_EXISTS_EXCEPTION);
        }
        userDTO.setPassword(encoder.encode(userDTO.getPassword()));
        User user = this.userRepository.saveAndFlush(mapper.map(userDTO, User.class));
        log.debug("successful user creation!");
        return mapper.map(user, UserDTO.class);
    }

    @Override
    public Page<UserDTO> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable).map(user -> mapper.map(user, UserDTO.class));
    }

    @Override
    public UserDTO getUserById(long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new EntityNotFoundException(ExceptionConstants.ENTITY_NOT_FOUND_EXCEPTION);
        }
        return mapper.map(user.get(), UserDTO.class);
    }

    @Override
    public UserDTO updateUser(UserDTO userDTO) throws EmailAlreadyUsedException {

        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (Objects.isNull(userDTO.getId()) || !existingUser.isPresent()) {
            throw new EntityNotFoundException(ExceptionConstants.ENTITY_NOT_FOUND_EXCEPTION);
        }
        if ((!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new EmailAlreadyUsedException(ExceptionConstants.USER_ALREADY_EXISTS_EXCEPTION);
        }
        existingUser.get().setModified(Instant.now());
        User updatedUser = userRepository.saveAndFlush(mapper.map(userDTO, User.class));
        log.debug("successful user update!");
        return mapper.map(updatedUser, UserDTO.class);
    }

    @Override
    public void delete(long id) {
        Optional<User> existingUser = userRepository.findById(id);
        if(!existingUser.isPresent()) {
            log.error("invalid User id: {}",id);
            throw new EntityNotFoundException(ExceptionConstants.ENTITY_NOT_FOUND_EXCEPTION);
        }
        userRepository.deleteById(id);
    }

    @Override
    public void syncUserData(String token, String email) {
        Optional<User> optionalUser = userRepository.findOneByEmailIgnoreCase(email);
        if (!optionalUser.isPresent()) {
            throw new EntityNotFoundException(ExceptionConstants.ENTITY_NOT_FOUND_EXCEPTION);
        }
        User user = optionalUser.get();
        user.setLastLogin(Instant.now());
        user.setToken(token);
        log.debug("updated user data  ");
    }
}
