package com.globallogic.userapp.service;

import com.globallogic.userapp.domain.dto.LoginDTO;

public interface AuthService {

    String authenticate(LoginDTO loginDTO);
}
