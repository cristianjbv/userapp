package com.globallogic.userapp.service;

import com.globallogic.userapp.domain.dto.UserDTO;
import com.globallogic.userapp.web.rest.exceptions.EmailAlreadyUsedException;
import com.globallogic.userapp.web.rest.exceptions.UserAlreadyExistsException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserService {


    UserDTO createUser(UserDTO userDTO) throws UserAlreadyExistsException;

    Page<UserDTO> getAllUsers(Pageable pageable);

    UserDTO getUserById(long id);

    UserDTO updateUser(UserDTO userDTO) throws EmailAlreadyUsedException;

    void delete(long id);

    void syncUserData(String token, String email);

}
