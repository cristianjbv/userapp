package com.globallogic.userapp.config;

public final class Constants {

    public static final String PASSWORD_REGEX = "^(?=.*?[A-Z])+(?=.*?[a-z])+(?=(?:.*[0-9]){2}).*";
    public static final String[] AUTH_WHITELIST = {
            // -- swagger ui
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/swagger-ui",
            "/swagger-ui/**",
            "/webjars/**"
    };

    private Constants() {
    }
}
