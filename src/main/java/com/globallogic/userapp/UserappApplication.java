package com.globallogic.userapp;

import com.globallogic.userapp.domain.Phone;
import com.globallogic.userapp.domain.User;
import com.globallogic.userapp.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class UserappApplication {

    private final UserRepository userRepository;


    private final PasswordEncoder bCryptPasswordEncoder;

    public UserappApplication(UserRepository userRepository, PasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;

        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public static void main(String[] args) {
        SpringApplication.run(UserappApplication.class, args);
    }


    /**
     * This method is executed right after app intialization
     * it populates memory database with the first user, this user information
     * corresponds to requirement document (exam) from GlobalLogic.
     */
    @PostConstruct
    public void initializeData(){

        List<Phone> phoneList = new ArrayList<>();
        User user = User.builder()
                .email("admin@globallogic.com")
                .name("admin")
                .password(this.bCryptPasswordEncoder.encode("Hunter22"))
                .active(true)
                .lastLogin(null)
                .created(Instant.now())
                .phones(phoneList)
                .modified(null).build();
        Phone phone = Phone.builder().cityCode(9L).countryCode(56L).number(77951232L).user(user).build();
        phoneList.add(phone);
        this.userRepository.save(user);
    }


}
