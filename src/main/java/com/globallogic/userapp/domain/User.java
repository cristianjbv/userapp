package com.globallogic.userapp.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.globallogic.userapp.config.Constants;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Getter
@Setter
public class User implements  Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Email(message = "Incorrect email format.")
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = true)
    private String email;

    @Pattern(regexp = Constants.PASSWORD_REGEX,message = "Incorrect password format.")
    @NotBlank
    private String password;

    @CreatedDate
    @Column(updatable = false)
    @Setter(value = AccessLevel.NONE)
    private Instant created = Instant.now();

    private Instant modified;

    private Instant lastLogin;

    private String token;

    private boolean active;


    @JsonBackReference
    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER )
    private List<Phone> phones;


}
