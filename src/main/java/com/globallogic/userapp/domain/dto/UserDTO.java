package com.globallogic.userapp.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.globallogic.userapp.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Email(message = "Incorrect email format.")
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = true)
    private String email;

    @NotBlank
    @Pattern(regexp = Constants.PASSWORD_REGEX, message = "Incorrect password format.")
    private String password;

    @ReadOnlyProperty
    private Instant created;

    private Instant modified;

    @JsonProperty("last_login")
    @ReadOnlyProperty
    private Instant lastLogin;

    private String token;

    private boolean active;

    private List<PhoneDTO> phones = new LinkedList<>();

}
