package com.globallogic.userapp.domain.dto;

import lombok.Data;

@Data
public class PhoneDTO {
    private Long number;
    private Long cityCode;
    private Long countryCode;
}
